package com.koushik.springreactiveapp.bootstrap;

import com.koushik.springreactiveapp.domain.Movie;
import com.koushik.springreactiveapp.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Component
public class BootstrapCLR implements CommandLineRunner {

    private final MovieRepository movieRepository;


    public BootstrapCLR(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        movieRepository.deleteAll().thenMany(
                Flux.just("Bahubali","Yatra")
                        .map(title->new Movie(title))
                        .flatMap(movieRepository::save))
                        .subscribe(null,null,()->{
                            movieRepository.findAll().subscribe(System.out::println);}
        );



    }
}
