package com.koushik.springreactiveapp.service;

import com.koushik.springreactiveapp.domain.Movie;
import com.koushik.springreactiveapp.domain.MovieEvent;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieService {

    Flux<MovieEvent> events(String movieId);

    Mono<Movie> getMovieById(String id);

    Flux<Movie> getAllMovies();
}
