package com.koushik.springreactiveapp.repositories;

import com.koushik.springreactiveapp.domain.Movie;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface MovieRepository extends ReactiveMongoRepository<Movie, String> {

}
